<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Majestic Motorsports</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://use.typekit.net/dek2umz.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">



<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo get_option('home'); ?>/wp-content/themes/Template/assets/styles/main.css" />
<link rel="stylesheet" type="text/css" href="<?php echo get_option('home'); ?>/wp-content/themes/Template/assets/styles/main2.css" />
<script src="<?php echo get_option('home'); ?>/wp-content/themes/Template/assets/scripts/script.js"></script>
<?php wp_head(); ?>
</head>
<body>
<header>
<?php include('nav.php'); ?>
<?php if( is_page('home')) { ?>
<!-- 	<div class="banner" style="background: url('<?php the_field('banner_image', 'options'); ?>'); background-size: cover; background-position: center;"></div> -->
	<?php include('carousel.php'); ?>
<?php } else { ?>
	<div class="banner page-banner" style="background: url('<?php the_field('banner_image', 'options'); ?>'); background-size: cover; background-position: center;">
	</div>
<?php } ?>

</header>