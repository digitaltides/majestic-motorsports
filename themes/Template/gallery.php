<?php get_header();
/*
Template Name: Gallery
*/ ?>
<div class="main-content container-fluid gallery-content">
	<div class="page-title"><?php the_title();?></div>
<?php
$images = get_field('gallery');

if( $images ): ?>
	<?php $index = 1; ?>
	<?php $totalNum = count( get_field('gallery') ); ?>
	<div class="row gray" style="padding-bottom: 0">
            <div class="content-width">
        <?php foreach( $images as $image ): ?>
            <div class="col-sm-4">
                <a style="    background: url('<?php echo $image['sizes']['medium']; ?>'); padding-top: 100%; width: 100%; display: inline-flex; background-position: center; background-size: fill; background-repeat: no-repeat" href="<?php echo $image['url']; ?>">
<!--                      <img src="<?php echo $image['sizes']['medium']; ?>" class="gallery-image" /> -->
                </a>
                <p><?php echo $image['caption']; ?></p>
            </div>
            <? if ($index % 3 == 0) : ?>
                        <? if ($index < $totalNum) : ?> 
                                </div>                           
                            </div><!-- .row -->
                            <div class="row gray px-0" style="padding: 0;">
                                        <div class="content-width">

                        <? elseif ($index == $totalNum) : ?>                            
                            </div><!-- .row -->
                        <? endif; ?>
                    <? endif; ?>
                <?php $index++; ?>
        <?php endforeach; ?>
<?php endif; ?>
</div>
</div>
</div>
<?php 
/*
*---------------------------------------------------------
* The code below was no longer sorting or removing images 
* once they were deleted, so we updated the code as seen 
* above, which fixed both of these problems.
*---------------------------------------------------------
<?php 
    $images = get_field('gallery');
        if( $images ): ?>
            <?php foreach( $images as $image ): ?>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <a href="<?php echo $image['url']; ?>">
                         <img src="<?php echo $image['sizes']['thumbnail']; ?>" class="aligncenter" />
                    </a>
                    <center>
                        <p><b><?php echo $image['caption']; ?></b><br />
                        <a href="<?php echo $image['title']; ?>" target="_blank"><?php echo $image['title']; ?></a></p>
                    </center>
                </div>
            <?php endforeach; ?>
        <?php endif; ?>
 
?>
*/ 
?>
<?php get_footer(); ?>