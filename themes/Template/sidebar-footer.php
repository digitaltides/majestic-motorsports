<div id="footer-primary" class="row footer footer-row-content-desktop">
    <?php if ( is_active_sidebar( 'footer' ) ) : ?>
        <?php dynamic_sidebar( 'footer' ); ?>
</div>
    <?php else : ?>
        <!-- Time to add some widgets! -->
        <?php endif; ?>
</div>