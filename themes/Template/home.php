<?php get_header();
/*
Template Name: Home
 */
?>
<main>

	<section id="home-content">
			<div class="mobile-search">
				<p>
					Search by Car
				</p>
			</div>
		<div class="mobile-content">
			<div class="parts-search">
					<?php echo do_shortcode('[turn14displayvmm]'); ?>
				</div>	
		</div>
			<div class="main-content container-fluid">
                <?php
$products = get_field('featured_products');
if ($products):
?>
                    <div class="row gray">
                        <h2 class="featured-section-title"><?php the_field('featured_products_title');?></h2>
                        <div class="featured-products products-slick">
                        <?php foreach ($products as $post): // variable must be called $post (IMPORTANT) ?>
		                            <?php setup_postdata($post);?>
		                            <div>
		                                <a class="feat-prod-cont" href="<?php the_permalink();?>" style="text-align: center">
		                                <?php if (has_post_thumbnail($post)):
        $thumbnail = get_the_post_thumbnail_url($post->ID);
    else:
        $thumbnail = 'http://majestic.local/wp-content/plugins/woocommerce/assets/images/placeholder.png';
    endif;?>
		                                <img src="<?php echo $thumbnail; ?>" alt="<?php the_title();?>" width="300" class="woocommerce-placeholder wp-post-image" style="width: 80%; text-align: center;">
		                                <h2 class="woocommerce-loop-product__title" style="color: white; text-decoration: none; text-align: center;"><?php the_title();?></h2>
		                                <p class="price">
		                                    <?php
    if ($product->is_on_sale()) {
        echo $product->get_sale_price();
    }
    echo $product->get_regular_price();
    ?>
		                                </p>
		                                <a href="/?add-to-cart=<?php echo $post->ID; ?>" data-quantity="1" class="feat-button" aria-label="Add “ACC ADARAC Truck Rack” to your cart" rel="nofollow">Add to cart</a>
		                                </a>
		                            </div>
		                        <?php endforeach;?>
                        </div>
                    </div>
                <?php wp_reset_postdata();
endif;?>
            </div>
                    <?php if (have_rows('brands')): ?>
            <div class="main-content container-fluid">
				<div class="row brands-section">
                    <h2 class="brands-section-title"><?php the_field('brands_title');?></h2>
					<div class="content-width brand-slick">
                    <?php
// loop through the rows of data
while (have_rows('brands')): the_row();
    $image = get_sub_field('image');?>
			                        <div class="brand-image">
			                            <a href="<?php the_sub_field('url');?>" >
				                            <img class="brand-img" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			                            </a>
			                        </div>
			                       <?php endwhile;?>
					</div>
				</div>
				</div>
        <?php endif;?>
            <div class="main-content container-fluid">
				<div class="row black">
					<div class="content-width">
						<?php the_field('info_section');?>
					</div>
				</div>
			</div>

                    <?php if (have_rows('services')): ?>
			<div class="main-content container-fluid">
				<div class="row gray services-slick">
                <?php while (have_rows('services')): the_row();
    $image = get_sub_field('image');
    $title = get_sub_field('title');
    $link = get_sub_field('link');
    ?>
		                        <div class="col-sm-4 service-home-cta">
		                            <a href="<?php echo $link; ?>" class="service-content-home-container" style="background: url('<?php echo $image; ?>'); background-position: center; background-size: cover;">
		                                <div class="service-content-home">
		                                    <h4><?php echo $title; ?></h4>
		                                    <p>Learn More</p>
		                                </div>
		                            </a>
		                        </div>
		                    <?php endwhile;?>
				</div>
            </div>
        <?php endif;?>
	</section>
</main>
<?php get_footer();?>