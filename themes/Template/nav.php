<nav class="main-nav" role="navigation">
                <!-- Mobile menu toggle button (hamburger/x icon) -->
                <input id="main-menu-state" type="checkbox" />
                <label class="main-menu-btn" for="main-menu-state">
                    <span class="main-menu-btn-icon"></span> Toggle main menu visibility
                </label>
                <h2 class="nav-brand"><a href="<?php echo get_option('home'); ?>"><img src="<?php the_field('header_logo', 'options'); ?>"></a></h2>
                <!-- Sample menu definition -->
                <div class="top-header">
                    <?php echo get_product_search_form(); ?>
                <?php if ( is_user_logged_in() ) { ?>
	            <a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" class="nav-links" title="<?php _e('My Account','woothemes'); ?>">My Account</a>
	            <?php } else { ?>
	            <a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" class="nav-links" title="<?php _e('Login | Register','woothemes'); ?>">
	              <?php _e('Login | Register','woothemes'); ?>
	            </a>
	            <?php } ?>
                </div>
                <?php 
                wp_nav_menu( array(
                    'theme_location'    => 'header-menu',
                    'menu' => 'header-menu',
                    'depth' => 2,
                    'container' => false,
                    'menu_class' => 'sm sm-clean',
                    'menu_id' => 'main-menu',
                    'walker' => new wp_bootstrap_navwalker())
                );
                ?>
<!--       <ul id="main-menu" class="sm sm-clean">
                    <li><a href="http://www.smartmenus.org/">Home</a></li>
                    <li><a href="http://www.smartmenus.org/about/">About</a>
                    <ul>
                        <li><a href="http://www.smartmenus.org/about/introduction-to-smartmenus-jquery/">Introduction to SmartMenus jQuery</a></li>
                        <li><a href="http://www.smartmenus.org/about/themes/">Demos + themes</a></li>
                        <li><a href="http://vadikom.com/about/#vasil-dinkov">The author</a></li>
                        <li><a href="http://www.smartmenus.org/about/vadikom/">The company</a>
                        <ul>
                            <li><a href="http://vadikom.com/about/">About Vadikom</a></li>
                            <li><a href="http://vadikom.com/projects/">Projects</a></li>
                            <li><a href="http://vadikom.com/services/">Services</a></li>
                            <li><a href="http://www.smartmenus.org/about/vadikom/privacy-policy/">Privacy policy</a></li>
                        </ul>
                        </li>
                    </ul>
                    </li>
                    <li><a href="http://www.smartmenus.org/download/">Download</a></li>
                    <li><a href="http://www.smartmenus.org/support/">Support</a>
                    <ul>
                        <li><a href="http://www.smartmenus.org/support/premium-support/">Premium support</a></li>
                        <li><a href="http://www.smartmenus.org/support/forums/">Forums</a></li>
                    </ul>
                    </li>
                    <li><a href="http://www.smartmenus.org/docs/">Docs</a></li>
                    <li><a href="http://www.smartmenus.org/contact/">Contact</a></li>
                </ul> -->
                </nav>