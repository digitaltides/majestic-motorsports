<div id="sidebar-primary" class="sidebar">
    <?php if ( is_active_sidebar( 'woosidebar' ) ) : ?>
        <h4 class="mob-title">Filters</h4>
        <div class="collapse">
        <?php dynamic_sidebar( 'woosidebar' ); ?>
</div>
    <?php else : ?>
        <!-- Time to add some widgets! -->
        <?php endif; ?>
</div>