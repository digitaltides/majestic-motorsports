<?php get_header(); 
/*
Template Name: Blog
*/
?>
<?php
  $temp = $wp_query;
  $wp_query= null;
  $wp_query = new WP_Query();
  $wp_query->query(''.'&paged='.$paged.'&posts_per_page=10');
?>
<div id="blog-content">
  <div class="container">  
      <div class="row">
        <div class="col-sm-12">
          <h1><?php the_title();?></h1>
        </div>
      </div>
          <?php if ($wp_query->have_posts()) : while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
            <?php 
            if ( has_post_thumbnail() ) {
              $cols = '9';
            }
          else {
                $cols = '12';
          } ?>
              <div class="row">
                  <div class="col-sm-3">
                      <?php echo get_the_post_thumbnail( $post_id, 'full', array( 'class' => 'aligncenter shadow top20' ) ); ?>
                  </div>
                  <div class="col-sm-<?php echo $cols; ?>">
                      <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                      <p>
                          <?php the_date(); ?><br />
                          <?php the_content_rss('', false, ' ' , 75); ?>
                          <a href="<?php the_permalink(); ?>"><span class="continue">Continue Reading</span></a>
                      </p>
                      </div> <!-- col-sm-9 -->
              </div> <!-- row --> 
          <?php endwhile; endif; ?> 
        <div class="row">
          <div class="col-sm-6">
            <p id="button"><?php next_posts_link( 'Older posts' ); ?></p>
          </div>
          <div class="col-sm-6">
            <p id="button" class="pull-right"><?php previous_posts_link( 'Newer posts' ); ?></p>
          </div>
        </div> 
  </div>
</div>
<?php get_footer(); ?>