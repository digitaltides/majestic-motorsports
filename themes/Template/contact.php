<?php get_header(); 
/* Template Name: Contact */
?>
<div class="main-content container-fluid contact-content">
	<div class="page-title"><?php the_title();?></div>
	<div class="row gray">
		<div class="content-width">
			<div class="contact-map">
				<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d209907.76365776337!2d-77.29198!3d34.710002!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89a904a66b38e9b9%3A0xb996de12301187fa!2s1041+Freedom+Way%2C+Hubert%2C+NC+28539!5e0!3m2!1sen!2sus!4v1536014632150" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
			<?php echo do_shortcode('[contact-form-7 id="44" title="Contact form 1"]'); ?>
		</div>
	</div>
</div>
<?php get_footer(); ?>