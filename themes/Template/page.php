<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

	<?php if( is_page( 'store' )) { ?>
		<div class="main-content container-fluid services-content">
				<div class="row">
						<?php include('loop.php'); ?>
				</div>
			</div>
		</div>
	<?php } else if( is_page( 'cart' )) { ?>
		<div class="main-content container-fluid services-content">
				<div class="page-title"><?php the_title();?></div>
				<div class="container">
				<div class="row">
						<?php include('loop.php'); ?>
				</div>
	</div>
			</div>
		</div>
	<?php } else if( '' !== get_post()->post_content ) { ?>
		<div class="main-content container-fluid">
			<div class="page-title"><?php the_title();?></div>
			<div class="row gray">
				<div class="content-width">
					<?php include('loop.php'); ?>
				</div>
			</div>
		</div>
	<?php } ?>

	<?php if(is_page( 'contact' )) { ?>
		<div class="main-content container-fluid contact-content">
			<div class="page-title"><?php the_title();?></div>
			<div class="row gray">
				<div class="content-width">
					<div class="contact-map">
						<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d209907.76365776337!2d-77.29198!3d34.710002!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89a904a66b38e9b9%3A0xb996de12301187fa!2s1041+Freedom+Way%2C+Hubert%2C+NC+28539!5e0!3m2!1sen!2sus!4v1536014632150" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
					</div>
					<?php echo do_shortcode('[contact-form-7 id="44" title="Contact form 1"]'); ?>
				</div>
			</div>
		</div>
	<?php } ?>

	<?php if(is_page( 'about' )) { ?>
		<div class="main-content container-fluid team-content">
		<div class="page-title"><?php the_title();?></div>
		<div class="row gray">
			<div class="content-width">
				<div class="col-sm-12 text-white">
					<?php the_field('who_we_are'); ?>
				</div>
			</div>
		</div>
		</div>
	<?php } ?>

	<?php if( is_page( 'services' )) { ?>
		<div class="main-content container-fluid services-content">
			<div class="page-title"><?php the_title();?></div>
			<div class="row gray">
				<div class="content-width content-push-right" id="service-dyno">
					<div class="col-md-12">
						<?php the_field('dyno_content'); ?>
					</div>
				</div>
			</div>
			<div class="row black">
				<div class="content-width content-push-right" id="service-engine-building">
					<div class="col-sm-12">
						<?php the_field('engine_building_content'); ?>
					</div>
				</div>
			</div>
			<div class="row gray">
				<div class="content-width content-push-right" id="service-suspension-setup">
					<div class="col-md-12">
						<?php the_field('suspension_setup_content'); ?>
					</div>
				</div>
			</div>
			<div class="row black">
				<div class="content-width content-push-right" id="service-custom-exhaust">
					<div class="col-sm-12">
						<?php the_field('custom_exhaust_content'); ?>
					</div>
				</div>
			</div>
			<div class="row gray">
				<div class="content-width content-push-right" id="service-paint-body">
					<div class="col-md-12">
						<?php the_field('paint_body_content'); ?>
					</div>
				</div>
			</div>
		</div>
	<?php } ?>

	<?php if( is_page( 'resources' )) { ?>
		<div class="main-content container-fluid services-content">
			<div class="page-title"><?php the_title();?></div>
			<div class="row gray">
				<div class="content-width" id="resource-faq">
					<div class="col-md-12">
						<?php the_field('faq_content'); ?>
					</div>
				</div>
			</div>
			<div class="row black">
				<div class="content-width" id="resource-catalog">
					<div class="col-sm-12">
						<?php the_field('catalog_content'); ?>
					</div>
				</div>
			</div>
			<div class="row gray">
				<div class="content-width" id="resource-links">
					<div class="col-md-12">
						<?php the_field('links_content'); ?>
					</div>
				</div>
			</div>
			<div class="row black">
				<div class="content-width" id="resource-ebay">
					<div class="col-sm-12">
						<?php the_field('ebay_content'); ?>
					</div>
				</div>
			</div>
			<div class="row gray">
				<div class="content-width" id="resource-financing">
					<div class="col-md-12">
						<?php the_field('financing_content'); ?>
					</div>
				</div>
			</div>
		</div>
	<?php } ?>



<?php get_footer(); ?>


