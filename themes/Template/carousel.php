<?php if( have_rows('slideshow_images') ) : ?>
<div id="carousel" class="carousel slide page-slider" data-ride="carousel" style="max-height: 60vh;">
    <div class="carousel-inner" role="listbox" style="max-height: 60vh;">
        <?php $i = 0; ?>
        <?php while( have_rows('slideshow_images') ) : the_row(); ?>
            <div style="background: url('<?php the_sub_field('image') ?>'); background-size: cover; background-position: center;" class="item banner <?php echo $i==0 ? 'active' : ''; $i++; ?>">
            </div>
        <?php endwhile; ?>
		<div class="parts-search-container">
						<div class="parts-search">
					<?php echo do_shortcode('[turn14displayvmm]'); ?>
				</div>	
		</div>

		<a class="left carousel-control" href=".carousel" role="button" data-slide="prev">
          <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href=".carousel" role="button" data-slide="next">
          <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
        
    </div>
</div>
<?php endif; ?>