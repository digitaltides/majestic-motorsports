<?php 
/* Template Name: Meet The Team */
get_header(); ?>
<div class="main-content container-fluid team-content">
<div class="page-title"><?php the_title();?></div>
<div class="row gray">
    <div class="content-width">
        <?php
        // check if the repeater field has rows of data
        if( have_rows('meet_the_team') ):
            // loop through the rows of data
            while ( have_rows('meet_the_team') ) : the_row(); ?>
                <div class="col-sm-3 team-member">
                    <img src="<?php the_sub_field('team_member_image'); ?>" class="team-member-img">
                    <h3><?php the_sub_field('team_member_name'); ?></h3>
                    <h5><?php the_sub_field('team_member_job_title'); ?></h5>
                    <p><?php the_sub_field('team_member_description'); ?></p>
                </div>
        <?php  endwhile;
        else :
            // no rows found
        endif; ?>
    </div>
</div>
<div class="row black">
    <div class="content-width">
        <div class="col-sm-12 ">
            <?php the_field('who_we_are'); ?>
        </div>
    </div>
</div>
<div class="row gray">
    <div class="content-width">
        <p class="section-title">Don't just take it from us, check out what our customers say:</p>
        <div class="col-md-4">
            <a href="<?php the_field('social_media_link_1'); ?>" class="social-link">
                <p><?php the_field('social_media_text_1'); ?></p>
            </a>
        </div>
        <div class="col-md-4">
            <a href="<?php the_field('social_media_link_2'); ?>" class="social-link">
                <p><?php the_field('social_media_text_2'); ?></p>
            </a>
        </div>
        <div class="col-md-4">
            <a href="<?php the_field('social_media_link_3'); ?>" class="social-link">
                <p><?php the_field('social_media_text_3'); ?></p>
            </a>
        </div>
    </div>
</div>
</div>
<?php get_footer(); ?>