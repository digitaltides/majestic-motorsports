<div id="rotator">
  <div class="wrapper">
    <div class="responsive-main">
      <?php
        if( have_rows('image') ):
            while ( have_rows('image') ) : the_row(); ?>
            <div class="slide" style="background:url('<?php the_sub_field('image'); ?>') center center no-repeat; background-size:cover;"></div>
            <?php endwhile;
          else :
          // no rows found
        endif;
      ?>      
    </div><!-- .responsive-main -->
  </div><!-- .wrapper -->
</div> <!-- #rotator -->