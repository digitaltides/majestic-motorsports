<?php 
/* Template Name: Services */
include('header.php'); ?>
<div class="main-content container-fluid serives-content">
            <div class="page-title">Services</div>
            <div class="row gray">
                <div class="content-width">
                    <div class="col-sm-4 service-menu-container">
                        <h3>Categories:</h3>
                        <ul class="service-menu">
                            <li><p id="service-dyno" class="service-link">Dyno</p></li>
                            <li><p id="service-engine-building" class="service-link">Engine-building</p></li>
                            <li><p id="service-suspension-setup" class="service-link">Suspension Setup</p></li>
                            <li><p id="service-custom-exhaust" class="service-link">Custom Exhaust</p></li>
                            <li><p id="service-paint-body" class="service-link">Paint/Body</p></li>
                        </ul>
                    </div>
                    <div class="col-sm-4">
                        <div class="service-dyno service-content">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                        </div>
                        <div class="service-engine-building service-content">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                        </div>
                        <div class="service-suspension-setup service-content">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                        </div>
                        <div class="service-custom-exhaust service-content">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                        </div>
                        <div class="service-paint-body service-content">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                        </div>
                    </div>
                    <div class="col-sm-4 service-photo" style="background: url('pexels-photo-209644'); background-size: cover; background-position: center;">
                        
                    </div>
                </div>
            </div>
            <div class="row gray">
                <div class="content-width">
                    <div class="col-md-12">

                    </div>
                </div>
            </div>
        </div>
<script>
$( "#service-dyno" ).click(function() {
  $( ".service-dyno" ).addClass('service-show');
});
</script>
<?php include('footer.php'); ?>