<?php

?>

<footer>
    <div class="content-footer">
        <div class="row-footer">
            <div class="footer-container">
                	<?php get_sidebar('footer');?>

                <!-- DESKTOP
                <div class="row footer-row-content-desktop">
                    <div class="col-sm-3 footer-contact-us">
                        <p>CONTACT US</p>
                        <ul>
                            <li><a href="#">1041 Freedom Way Hubert, NC 28539</a></li>
                            <li><a href="tel:9102194217">T: (910) 219-4217</a></li>
                            <li><a href="fax:9102194249">F: (910) 219-4249</a></li>
                            <li><a href="mailto:sales@majesticmotorsports.net">sales@majesticmotorsports.net</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-2 footer-categories">
                        <p>CATEGORIES</p>
                        <ul>
                            <li><a href="#">Who we are</a></li>
                            <li><a href="#">Meet the Team</a></li>
                            <li><a href="#">Training</a></li>
                            <li><a href="#">Gallery</a></li>
                            <li><a href="#">Contact</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-2 footer-services">
                        <p>SERVICES</p>
                        <ul>
                            <li><a href="#">All Wheel Drive Dyno (AWD)</a></li>
                            <li><a href="#">Engine Building</a></li>
                            <li><a href="#">Suspension Setup</a></li>
                            <li><a href="#">Custom Exhaust</a></li>
                            <li><a href="#">Paint/Body</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-2 footer-resources">
                        <p>RESOURCES</p>
                        <ul>
                            <li><a href="#">FAQ</a></li>
                            <li><a href="#">Videos</a></li>
                            <li><a href="#">Catalog</a></li>
                            <li><a href="#">Links</a></li>
                            <li><a href="#">eBay Auction</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-3 footer-newsletter">
                        <p>Newsletter Signup</p>
                        <p>Enter your email address below to receive our weekly newsletter and possible deals!</p>
                        <input class="footer-enter-email" type="text" placeholder="Email Address">
                        <input class="footer-subscribe-btn" type="submit" value="Subscribe">
                    </div>
                </div> -->
                
                <!-- <div class="row footer-mobile-nav">
                    <ul>
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Terms</a></li>
                        <li><a href="#">Privacy Policy</a></li>
                        <li><a href="#">Contact</a></li>
                    </ul>
                </div> -->

            </div>
        
            <div class="row-branding font-sm">
                <div>
                    <p><a href="#">Website Designed & Developed by Digital Tides</a></p>
                    <p>Copyright 2018 Majestic Motorsports</p>
                </div>
            </div>
        </div>
    </div>
</footer>
</body>
<?php wp_footer(); ?>
</html>