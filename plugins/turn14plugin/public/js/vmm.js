jQuery(document).ready(function ($) {
    // We'll pass this variable to the PHP function example_ajax_request
    $(document).on('change', '#year', function () {
        $('#make').empty();
        $('#make').prop("disabled", true)
        $('#make').append('<option>SELECT A MAKE</option>');
        $('#model').empty();
        $('#model').prop("disabled", true)
        $('#model').append('<option>SELECT A MODEL</option>');

        var year = $('#year').val();
        console.log(year)
        // This does the ajax request
        $.ajax({
            url: turn_ajax.ajax_url,
            data: {
                action: 'turn14_ajax_get_makes',
                year: year
            },
            success: function (data) {
                console.log('data')
                console.log(data)
                data = JSON.parse(data);
                $('#make').empty();
                $('#make').prop("disabled", false)
                // This outputs the result of the ajax request
                var n = 0;
                if(data.length){
                    for (make in data) {
                        console.log(data[make]);
                        makeData = data[make];
                        if (n == 0) {
                            $('#make').append('<option>CHOOSE A MAKE</option>')
                        }
                        $('#make').append('<option value="' + makeData['MakeID'] + '">' + makeData['MakeName'] + '</option>')
                        n++
                    }
                } else {
                    $('#make').prop("disabled", true)
                    $('#make').append('<option>No Makes Found</option>')
                }
            },
            error: function (errorThrown) {
                console.log(errorThrown);
            }
        });
    });
    // We'll pass this variable to the PHP function example_ajax_request
    $(document).on('change', '#make', function () {
        var make = $('#make').val();
        var year = $('#year').val();
        console.log(model);
        console.log(year)
        // This does the ajax request
        $.ajax({
            url: turn_ajax.ajax_url,
            data: {
                action: 'turn14_ajax_get_models',
                year: year,
                make: make
            },
            success: function (data) {
                console.log(data)
                data = JSON.parse(data);
                $('#model').empty();
                $('#model').prop("disabled", false)
                var n = 0;
                // This outputs the result of the ajax request
                for (model in data) {
                    console.log(data[model]);
                    modelData = data[model];
                    if(n == 0){
                        $('#model').append('<option>CHOOSE A MODEL</option>')
                    }
                    console.log($("#model").val());
                    $('#model').append('<option value="' + modelData['ModelID'] + '">' + modelData['ModelName'] + '</option>')
                    n++;
                }
            },
            error: function (errorThrown) {
                console.log(errorThrown);
            }
        });
    });

    $('#vmmSubmit').click(function(e){
        e.preventDefault();
        var make = $('#make').val();
        var year = $('#year').val();
        var model = $('#model').val();
        console.log(model);
        console.log(year)
        console.log(model)

        // This does the ajax request
        $.ajax({
            url: turn_ajax.ajax_url,
            data: {
                action: 'turn14_ajax_submit',
                year: year,
                make: make,
                model: model
            },
            success: function (data) {
                console.log(data)
                data = JSON.parse(data)
                var vehicles = data['vehicles'];
                console.log(vehicles)
                vehicles = JSON.parse(vehicles);

                console.log(vehicles[0])
                window.location = data["url"] + '/vehicle/' + vehicles[0]['VehicleID'];

            },
            error: function (errorThrown) {
                console.log(errorThrown);
            }
        });
    })

});