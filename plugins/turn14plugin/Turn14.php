<?php
/*
Plugin Name:  Turn 14 Plugin
Description:  Plugin to import Turn 14 Products into the WooCommerce DB
Plugin URI:   https://digitaltides.agency/turn14
Author:       Digital Tides, LLC
Version:      1.0
Text Domain:  turn14
Domain Path:  /languages
License:      GPL v2 or later
License URI:  https://www.gnu.org/licenses/gpl-2.0.txt
 */
// exit if file is called directly
if (!defined('ABSPATH')) {
    exit;
}

require plugin_dir_path(__File__) . 'wp-background-processing/wp-background-processing.php';

/**
 * Check if WooCommerce is active
 **/
if (in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins')))) {
    function first_run()
    {
        $pluginExists = get_option('firstRun');
        if ($pluginExists && $pluginExists !== 0) {
            return; // do nothing
        } else if (!$pluginExists) {
            add_option('firstRun', '0');
        }
    }
    register_activation_hook(__FILE__, 'first_run');

    /**
     * Enqueues plugin-specific scripts.
     */
    function enqueue_turn_scripts()
    {
        wp_enqueue_script('turn-scripts', plugins_url('public/js/vmm.js', __FILE__), array('jquery'), null, true);
        wp_localize_script('turn-scripts', 'turn_ajax', array(
            'ajax_url' => admin_url('admin-ajax.php'),
        )
        );
    }
    add_action('wp_enqueue_scripts', 'enqueue_turn_scripts');

    /**
     * Enqueues plugin-specific scripts.
     */
    function admin_enqueue_turn_scripts()
    {
        //$access_token = get_turn14_accessToken()['access_token'];
        //wp_enqueue_script('turn-scripts', plugins_url('admin/js/brands.js', __FILE__), array('jquery'), null, true);
        //wp_localize_script('turn-scripts', 'turn_ajax', array(
        //    'ajax_url' => admin_url('admin-ajax.php'),
        //    'access_token' => $access_token
        //)
        //);
    }
    add_action('admin_enqueue_scripts', 'admin_enqueue_turn_scripts');
    
    function turn_init()
    {
        // create a new taxonomy
        register_taxonomy(
            'product_fitments',
            'product',
            array(
                'label' => __('Fitment'),
                'rewrite' => array('slug' => 'vehicle'),
                'hierarchical' => true,
                'query_var' => true,
                'show_ui' => true,
            )
        );
        register_taxonomy(
            'product_brands',
            'product',
            array(
                'label' => __('Brands'),
                'rewrite' => array('slug' => 'brand'),
                'hierarchical' => true,
                'query_var' => true,
                'show_ui' => true,
            )
        );

    }
    add_action('init', 'turn_init');
    // load text domain
    function turn14_load_textdomain()
    {

        load_plugin_textdomain('turn14', false, plugin_dir_path(__FILE__) . 'languages/');

        if (!wp_next_scheduled('turn14_run_import')) {
            wp_schedule_event(1539648000, 'hourly', 'turn14_run_import');
        }

    }
    add_action('plugins_loaded', 'turn14_load_textdomain');
	   // require_once plugin_dir_path(__FILE__) . 'includes/import-engine.php';
    require_once plugin_dir_path(__FILE__) . 'includes/import-update-joint.php';
require_once plugin_dir_path(__FILE__) . 'includes/update-engine.php';
require_once plugin_dir_path(__FILE__) . 'includes/shipping-functions.php';

    require_once plugin_dir_path(__FILE__) . 'includes/core-functions.php';
        function get_turn14_accessToken()
    {
        $options = get_option('turn14_options');
        $client_id = $options['turn14_client_id'];
        $client_secret = $options['turn14_client_secret'];
        $urlRoot = isset($options['turn14_testing']) ? 'https://apitest.turn14.com' : 'https://api.turn14.com';

        $url = $urlRoot . "/v1/token";
        $fields = array(
            'client_id' => $client_id,
            'client_secret' => $client_secret,
            'grant_type' => 'client_credentials',
        );

        $fields_string = '';
        //url-ify the data for the POST
        foreach ($fields as $key => $value) {$fields_string .= $key . '=' . $value . '&';}
        rtrim($fields_string, '&');
        //open connection
        $ch = curl_init();

        //set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);

        //execute post
        $result = curl_exec($ch);
        $json = json_decode($result, true);
        //close connection
        curl_close($ch);

        return $json;
    }
        // default plugin options
    function turn14_options_default()
    {

        return array(
        );

    }
    // include plugin dependencies: admin only
    if (is_admin()) {
        $firstRun = get_option('firstRun');
        if ($firstRun == 0) {
            function admin_notice_firstRun()
            {
                ?>
			<div class="notice notice-success is-dismissible">
				<p><?php _e('Sweet! Lets get you hooked up to the Turn 14 API', 'turn14');?></p>
				<a class="button" style="margin-bottom: 20px;" href="options-general.php?page=turn14">Let's Go!</a>
			</div>
			<?php
}
            add_action('admin_notices', 'admin_notice_firstRun');
        }

        // include plugin dependencies: admin and public
        require_once plugin_dir_path(__FILE__) . 'admin/admin-menu.php';
        require_once plugin_dir_path(__FILE__) . 'admin/settings-page.php';
        require_once plugin_dir_path(__FILE__) . 'admin/settings-register.php';
        require_once plugin_dir_path(__FILE__) . 'admin/settings-callbacks.php';
        require_once plugin_dir_path(__FILE__) . 'admin/settings-validate.php';
        require_once plugin_dir_path(__FILE__) . 'admin/brands/settings-page.php';
        require_once plugin_dir_path(__FILE__) . 'admin/brands/settings-register.php';
        require_once plugin_dir_path(__FILE__) . 'admin/brands/settings-callbacks.php';
        require_once plugin_dir_path(__FILE__) . 'admin/brands/settings-validate.php';
                require_once plugin_dir_path(__FILE__) . 'includes/import-ajax.php';


    }


    function post_request_turn14($url, $fields, $access_token)
    {

        $url .= $access_token;

        $fields_string = '';
        //url-ify the data for the POST
        foreach ($fields as $key => $value) {$fields_string .= $key . '=' . $value . '&';}
        rtrim($fields_string, '&');
        //open connection
        $ch = curl_init();

        //set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);

        //execute post
        $result = curl_exec($ch);
        $json = json_decode($result, true);
        //close connection
        curl_close($ch);

        return $json;
    }

    function get_request_turn14($url, $access_token)
    {

        $url .= $access_token;

        // create curl resource
        $ch = curl_init();

        // set url
        curl_setopt($ch, CURLOPT_URL, $url);

        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        // $result contains the output string
        $result = curl_exec($ch);
        $json = json_decode($result, true);

        // close curl resource to free up system resources
        curl_close($ch);

        return $json;
    }
    function get_brands_turn14($urlRoot, $access_token)
    {

        $url = $urlRoot . '/v1/brands?access_token=' . $access_token;

        // create curl resource
        $ch = curl_init();

        // set url
        curl_setopt($ch, CURLOPT_URL, $url);

        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        // $result contains the output string
        $result = curl_exec($ch);
        $json = json_decode($result, true);

        // close curl resource to free up system resources
        curl_close($ch);

        $brandsArr = array();

        foreach ($json as $brands) {
            foreach ($brands as $brand) {
                $brandArr = array(
                    'termID' => $brand['id'],
                    'termName' => $brand['attributes']['name'],
                );
                array_push($brandsArr, $brandArr);
            }
        }

        return $brandsArr;
    }


    // default plugin options
    function turn14_brands_default()
    {

        return array();

    }
} else {
    function admin_notice_error_wc()
    {
        deactivate_plugins('/turn14plugin/Turn14.php');
        ?>
    <div class="notice notice-error is-dismissible">
        <p><?php _e('Looks like you need Woocommerce!', 'turn14');?></p>
    </div>
    <?php
}
    add_action('admin_notices', 'admin_notice_error_wc');
}
