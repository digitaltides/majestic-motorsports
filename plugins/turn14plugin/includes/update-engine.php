<?php
add_action('turn14_run_import', 'turn14_update_objects');

function turn14_update_objects()
{
    $options = get_option('turn14_options', turn14_options_default());
    $brandsArray = get_option('turn14_brands', turn14_brands_default());
    $urlRoot = isset($options['turn14_testing']) ? 'https://apitest.turn14.com' : 'https://api.turn14.com';

    $access_token = get_turn14_accessToken()['access_token'];
    $url = $urlRoot .'/v1/items/updates?access_token=' . $access_token;
    // create curl resource
    $ch = curl_init();

// set url
    curl_setopt($ch, CURLOPT_URL, $url);

//return the transfer as a string
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

// $result contains the output string
    $result = curl_exec($ch);
    $json = json_decode($result, true);
// close curl resource to free up system resources
    curl_close($ch);

    if ($json['meta']['total_pages'] > 1) {
        $pages = $json['meta']['total_pages'];
        for ($page = 1; $page <= $pages; $page++) {
            $url = $urlRoot . "/v1/items/updates?page=" . $page . "access_token=" . $access_token;
            // create curl resource
            $ch = curl_init();

            // set url
            curl_setopt($ch, CURLOPT_URL, $url);

            //return the transfer as a string
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

            // $result contains the output string
            $result = curl_exec($ch);
            $jsonSub = json_decode($result, true);
            array_push($json, $jsonSub);
            // close curl resource to free up system resources
            curl_close($ch);
        }
        foreach ($json['data'] as $item) {
            if ($item['type'] == 'Item') {
                $on_brand = array_search($item['attributes']['brand_id'], $brandsArray );
                if ($on_brand) {
                    turn14_item_data($item, $access_token, $brands);
                }
            }
        }
    } else {
        foreach ($json['data'] as $item) {
            if ($item['type'] == 'Item') {
                $on_brand = array_search($item['attributes']['brand_id'], $brandsArray);
                if ($on_brand) {
                    turn14_item_data($item, $access_token, $brands);
                }
            }
        }
    }

}
