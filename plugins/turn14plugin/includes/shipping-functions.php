<?php
function turn14_shipping_method_init()
{
    if (!class_exists('WC_Turn14_Shipping')) {
        class WC_Turn14_Shipping extends WC_Shipping_Method
        {
            /**
             * Constructor for your shipping class
             *
             * @access public
             * @return void
             */
            public function __construct()
            {
                $this->id = 'turn14_shipping'; // Id for your shipping method. Should be unique.
                $this->method_title = __('Turn 14 Shipping'); // Title shown in admin
                $this->method_description = __('Shipping methods returned by !urn 14'); // Description shown in admin
                $this->enabled = "yes"; // This can be added as an setting but for this example its forced enabled
                $this->title = "Turn14 Shipping"; // This can be added as an setting but for this example its forced.
                $this->init();
            }
            /**x
             * Init your settings
             *
             * @access public
             * @return void
             */
            function init()
            {
                // Load the settings API
                $this->init_form_fields(); // This is part of the settings API. Override the method to add your own settings
                $this->init_settings(); // This is part of the settings API. Loads settings you previously init.
                // Save settings in admin if you have any defined
                add_action('woocommerce_update_options_shipping_' . $this->id, array($this, 'process_admin_options'));
            }
            /**
             * calculate_shipping function.
             *
             * @access public
             * @param mixed $package
             * @return void
             */
            public function calculate_shipping($package = array())
            {
                $turn_options = get_option('turn14_options', turn14_options_default());
$urlRoot = isset($turn_options['turn14_testing']) ? 'https://apitest.turn14.com' : 'https://api.turn14.com';

                global $wpdb;
                $access_token = get_turn14_accessToken()['access_token'];
                error_log(json_encode($package['user']['ID']));

                $user_id = $package['user']['ID'];

                $user_info = get_userdata($user_id);
                $first_name = $user_info->first_name;
                $last_name = $user_info->last_name;
                $email = $user_info->user_email;
                $destination = $package['destination'];
                $items = $package['contents'];
                $itemsArray = array();
                foreach ($items as $item) {
                    $product_id = $item['product_id'];
                    $product = wc_get_product($product_id);
                    $sku = $product->get_sku();
                    $quantity = $item["quantity"];
                    $itemArr = array(
                        'item_identifier' => $sku,
                        'item_identifier_type' => 'part_number',
                        'quantity' => $quantity,
                    );
                    array_push($itemsArray, $itemArr);
                }
                $build_request = array(
                    'data' => array(
                        'environment' => 'testing',
                        'po_number' => rand(),
                        'order_notes' => 'Woocommerce Shipping Quote',
                        'locations' => array(
                            array(
                                'location' => 'default',
                                'combine_in_out_stock' => true,
                                'items' => $itemsArray,
                            ),
                        ),
                        'recipient' => array(
                            'name' => $first_name . ' ' . $last_name,
                            'address' => $destination['address'],
                            'address_2' => $destination['address_2'],
                            'city' => $destination['city'],
                            'state' => $destination['state'],
                            'country' => $destination['country'],
                            'zip' => $destination['postcode'],
                            'email' => $email,
                            'phone_number' => get_user_meta($user_id, 'billing_phone', true),
                            'is_shop_address' => false,
                        ),
                    ),
                );

                $fields = $build_request;
//url-ify the data for the POST
                $fields_string = json_encode($build_request);
//open connection
                $url = $urlRoot . '/v1/quote?access_token=' . $access_token;

//set the url, number of POST vars, POST data
                $ch = curl_init($url);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($fields_string))
                );

//execute post
                $result = curl_exec($ch);
                error_log($result);

                $shipments = json_decode($result, true);
//close connection
                curl_close($ch);
                $json = $shipments['data'];
                $shipment = $json['attributes']['shipment'];
                $ship_options = $shipment[0]['shipping'];
                $url = $urlRoot . '/v1/shipping?access_token=' . $access_token;

// create curl resource
                $ch = curl_init();

// set url
                curl_setopt($ch, CURLOPT_URL, $url);

//return the transfer as a string
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

// $result contains the output string
                $shipping_code = curl_exec($ch);
                error_log($shipping_code);
                $shipping_codes = json_decode($shipping_code, true);
                $shipping_codes_data = $shipping_codes['data'];
// close curl resource to free up system resources
                curl_close($ch);

                if($ship_options):
                foreach ($ship_options as $ship) {
                    error_log($ship['shipping_code']);
                    error_log(json_encode($shipping_codes_data[$ship['shipping_code']]['attributes']['transportation_name']));
                    $rate = array(
                        'id' => $ship['shipping_quote_id'],
                        'label' => $shipping_codes_data[$ship['shipping_code']]['attributes']['transportation_name'],
                        'cost' => $ship['cost'],
                        'calc_tax' => 'per_item',
                    );

                    // Register the rate
                    $this->add_rate($rate);
                }
                endif;
            }
        }
    }
}
add_action('woocommerce_shipping_init', 'turn14_shipping_method_init');
function add_turn14_methods($methods)
{
    $methods['turn14_shipping'] = 'WC_Turn14_Shipping';
    return $methods;
}
add_filter('woocommerce_shipping_methods', 'add_turn14_methods');
