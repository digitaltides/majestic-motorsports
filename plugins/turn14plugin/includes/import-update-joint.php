<?php

function turn14_item_data($item, $access_token, $brand)
{
    $turn_options = get_option('turn14_options', turn14_options_default());
    $urlRoot = isset($turn_options['turn14_testing']) ? 'https://apitest.turn14.com' : 'https://api.turn14.com';
    
    $url = $urlRoot . '/v1/items/data/' . $item['id'] . '?access_token=' . $access_token;
    // create curl resource
    $ch = curl_init();
    // set url
    curl_setopt($ch, CURLOPT_URL, $url);

    //return the transfer as a string
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    // $result contains the output string
    $result = curl_exec($ch);
    $result = json_decode($result, true);
    array_push($item, $result);

    // close curl resource to free up system resources
    curl_close($ch);

    turn14_import_get_pricing($item, $access_token, $brand);

}

function turn14_import_get_pricing($item, $access_token, $brand)
{
    $turn_options = get_option('turn14_options', turn14_options_default());
	$urlRoot = isset($turn_options['turn14_testing']) ? 'https://apitest.turn14.com' : 'https://api.turn14.com';
	$access_token = get_turn14_accessToken()['access_token'];
    $options = get_option('turn14_options', turn14_options_default());
    $url = $urlRoot . '/v1/pricing/' . $item["id"] . '?access_token=' . $access_token;
    // create curl resource
    $ch = curl_init();
    // set url
    curl_setopt($ch, CURLOPT_URL, $url);

    //return the transfer as a string
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    // $result contains the output string
    $result = curl_exec($ch);
    $result = json_decode($result, true);
    $result = $result['data'];
    if ($result['attributes']['has_map'] == true) {
        $markup = (int)$turn_options['turn14_pricing'] / 100;
        $markup = 1 + $markup;
		$rawPrice = (int)$result['attributes']['purchase_cost'];
        $price = $rawPrice * $markup;
		var_dump($result);
		var_dump($item['id']);
		var_dump($price);
		var_dump($markup);
        $map = 0;
        foreach ($result['attributes']['pricelists'] as $key => &$pricelist) {
            if ($pricelist['name'] = 'MAP') {
                $map = $pricelist['price'];
                break;
            }
        }
        if ($map > $price) {
            $price = $map;
        }
        turn14_insert_into_db($item, $price, $brand);
    } else {
        $markup = (int)$turn_options['turn14_pricing'] / 100;
        $markup = 1 + $markup;
		$rawPrice = (int)$result['attributes']['purchase_cost'];
        $price = $rawPrice * $markup;
		var_dump($result);
		var_dump($item['id']);
		var_dump($price);
		var_dump($markup);
        turn14_insert_into_db($item, $price, $brand);
    }

}
function attach_image($fileurl, $filealt, $post_id)
{
    // only need these if performing outside of admin environment
    require_once ABSPATH . 'wp-admin/includes/media.php';
    require_once ABSPATH . 'wp-admin/includes/file.php';
    require_once ABSPATH . 'wp-admin/includes/image.php';
    // magic sideload image returns an HTML image, not an ID
    $media = media_sideload_image($fileurl, $post_id);

    // therefore we must find it so we can set it as featured ID
    if (!empty($media) && !is_wp_error($media)) {
        $args = array(
            'post_type' => 'attachment',
            'posts_per_page' => -1,
            'post_status' => 'any',
            'post_parent' => $post_id,
        );

        // reference new image to set as featured
        $attachments = get_posts($args);

        if (isset($attachments) && is_array($attachments)) {
            foreach ($attachments as $attachment) {
                // grab source of full size images (so no 300x150 nonsense in path)
                $image = wp_get_attachment_image_src($attachment->ID, 'full');
                // determine if in the $media image we created, the string of the URL exists
                if (strpos($media, $image[0]) !== false) {
                    // if so, we found our image. set it as thumbnail
                    set_post_thumbnail($post_id, $attachment->ID);
                    unset($attachments);
                    // only want one image
                    break;
                }
            }
        }
    }
}
function turn14_insert_into_db($item, $price, $brand)
{
    global $wpdb;
    $sku = $item['attributes']['part_number'];
    $product_id = $wpdb->get_var($wpdb->prepare("SELECT post_id FROM $wpdb->postmeta WHERE meta_key='_sku' AND meta_value='%s' LIMIT 1", $sku));
    if ($product_id) {
        $post_id = $product_id;
        update_post_meta($post_id, '_visibility', 'visible');
        update_post_meta($post_id, '_stock_status', 'instock');
        update_post_meta($post_id, 'total_sales', '0');
        update_post_meta($post_id, '_downloadable', 'no');
        update_post_meta($post_id, '_virtual', 'no');
        update_post_meta($post_id, '_regular_price', $price);
        update_post_meta($post_id, '_sale_price', '');
        update_post_meta($post_id, '_purchase_note', '');
        update_post_meta($post_id, '_featured', 'no');
        update_post_meta($post_id, '_weight', $item['attributes']['dimensions'][0]['weight']);
        update_post_meta($post_id, '_length', $item['attributes']['dimensions'][0]['length']);
        update_post_meta($post_id, '_width', $item['attributes']['dimensions'][0]['width']);
        update_post_meta($post_id, '_height', $item['attributes']['dimensions'][0]['height']);
        update_post_meta($post_id, '_sku', $item['attributes']['part_number']);
        update_post_meta($post_id, '_product_attributes', array());
        update_post_meta($post_id, '_sale_price_dates_from', '');
        update_post_meta($post_id, '_sale_price_dates_to', '');
        update_post_meta($post_id, '_price', $price);
        update_post_meta($post_id, '_sold_individually', '');
        update_post_meta($post_id, '_manage_stock', 'no');
        update_post_meta($post_id, '_backorders', 'no');
        update_post_meta($post_id, '_stock', '');
        if (isset($item['attributes']['thumbnail'])) {
            $attachment_id = attach_image($item['attributes']['thumbnail'], $item['attributes']['part_description'], $post_id);
            add_post_meta($post_id, '_thumbnail_id', $attachment_id);
        }
        categories($item, $post_id, $brand);
    } else {
        $post_id = wp_insert_post(array(
            'post_author' => 2,
            'post_title' => $item['attributes']['product_name'] . ' - ' . $item['attributes']['part_number'] ,
			'post_name' => $item['attributes']['part_number'],
            'post_content' => $item['attributes']['part_description'],
            'post_status' => 'publish',
            'post_type' => "product",
        ));
        wp_set_object_terms($post_id, 'simple', 'product_type');
        update_post_meta($post_id, '_visibility', 'visible');
        update_post_meta($post_id, '_stock_status', 'instock');
        update_post_meta($post_id, 'total_sales', '0');
        update_post_meta($post_id, '_downloadable', 'no');
        update_post_meta($post_id, '_virtual', 'no');
        update_post_meta($post_id, '_regular_price', $price);
        update_post_meta($post_id, '_sale_price', '');
        update_post_meta($post_id, '_purchase_note', '');
        update_post_meta($post_id, '_featured', 'no');
        update_post_meta($post_id, '_weight', $item['attributes']['dimensions'][0]['weight']);
        update_post_meta($post_id, '_length', $item['attributes']['dimensions'][0]['length']);
        update_post_meta($post_id, '_width', $item['attributes']['dimensions'][0]['width']);
        update_post_meta($post_id, '_height', $item['attributes']['dimensions'][0]['height']);
        update_post_meta($post_id, '_sku', $item['attributes']['part_number']);
        update_post_meta($post_id, '_product_attributes', array());
        update_post_meta($post_id, '_sale_price_dates_from', '');
        update_post_meta($post_id, '_sale_price_dates_to', '');
        update_post_meta($post_id, '_price', $price);
        update_post_meta($post_id, '_sold_individually', '');
        update_post_meta($post_id, '_manage_stock', 'no');
        update_post_meta($post_id, '_backorders', 'no');
        update_post_meta($post_id, '_stock', '');

        categories($item, $post_id, $brand);

    }
}

function categories($item, $post_id, $brand)
{
    $cat = $item['attributes']['category'];
    $cat_name = $item['attributes']['category'];
    $sub_cat = $item['attributes']['subcategory'];
    $sub_cat_name = $item['attributes']['subcategory'];

    $catExists = term_exists($cat, 'product_cat');
    if ($catExists != null) {
        $subcatExists = term_exists($cat, 'product_cat', $catExists);
        if ($subcatExists) {
            $seoURL_cat = seoUrl($cat);
            $seoURL_subcat = seoUrl($sub_cat);
            wp_set_object_terms($post_id, array($seoURL_cat, $seoURL_subcat), 'product_cat');
            fitment($item, $post_id, $brand);
        } else {
            $seoURL_cat = seoUrl($cat);
            $seoURL_subcat = seoUrl($sub_cat);
            $subcat = wp_insert_term(
                $sub_cat_name, // the term
                'product_cat', // the taxonomy
                array(
                    'slug' => seoUrl($sub_cat),
                    'parent' => $catExists,
                )
            );
            wp_set_object_terms($post_id, array($seoURL_cat, $seoURL_subcat), 'product_cat');
            fitment($item, $post_id, $brand);
        }
    } else {
        $cat_slug = seoUrl($cat);
        $cat = wp_insert_term(
            $cat_name, // the term
            'product_cat', // the taxonomy
            array(
                'slug' => $cat_slug,
            )
        );
        $subCat_slug = seoUrl($sub_cat);
        $subcat = wp_insert_term(
            $sub_cat_name, // the term
            'product_cat', // the taxonomy
            array(
                'slug' => $subCat_slug,
                'parent' => $catExists,
            )
        );
        wp_set_object_terms($post_id, array($cat_slug, $subCat_slug), 'product_cat');
        fitment($item, $post_id, $brand);
    }
}
function fitment($item, $post_id, $brand)
{
    $itemData = $item[0]['data'];
    if(count($itemData) > 0):
    $itemData = $item[0]['data'][0];
    if (array_key_exists("vehicle_fitments", $itemData)) {
        $vehicles = $itemData["vehicle_fitments"];
        $vehicleArray = (array) [];
        foreach ($vehicles as $vehicle) {
            $vehicle_id = (string)$vehicle['vehicle_id'];
            $vehIDExists = term_exists($vehicle_id, 'product_fitments');
            if ($vehIDExists) {
				array_push($vehicleArray, $vehicle_id);
				sleep(3);
            } else {
                $subcat = wp_insert_term(
                    $vehicle_id, // the term
                    'product_fitments', // the taxonomy
                    array(
                        'slug' => seoUrl($vehicle_id)
                    )
                );
                array_push($vehicleArray, $vehicle_id);
                sleep(1);
            }
            wp_set_object_terms($post_id, $vehicleArray, 'product_fitments');
        }
    }
    endif;
	brand($item, $post_id, $brand);
    
}

function brand($item, $post_id, $brand)
{
    $termExists = term_exists($brand, 'product_brands');
    if ($termExists) {
        wp_set_object_terms($post_id, $brand, 'product_brands');
    } else {
        $brand = wp_insert_term(
            $item['attributes']['brand'], // the term
            'product_brands', // the taxonomy
            array(
                'slug' => $brand,
            )
        );
        wp_set_object_terms($post_id, $brand, 'product_brands');
    }
}

function add_gallery($items, $post_id)
{
    $itemData = $item[0]['data'][0];
    $ids = array();
    $images = $itemData['files'];
    foreach ($images as $image) {
		$primaryCt = 0;
        if ($image['type'] == 'Image') {
            $largest = count($image) - 1;
            $attachment_id = attach_image($image[$largest]['url'], $item['attributes']['product_name'], $post_id);
			array_push($ids, $attachment_id);
			if($image['media_content'] == 'Photo - Primary' && $primaryCt == 0){
            	add_post_meta($post_id, '_thumbnail_id', $attachment_id);
				$primaryCt .= 1;
			}
        }
    }

    add_post_meta($post_id, '_product_image_gallery', implode(',', $ids));

}

function seoUrl($string)
{
    //Lower case everything
    $string = strtolower($string);
    //Make alphanumeric (removes all other characters)
    $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
    //Clean up multiple dashes or whitespaces
    $string = preg_replace("/[\s-]+/", " ", $string);
    //Convert whitespaces and underscore to dash
    $string = preg_replace("/[\s_]/", "-", $string);
    return $string;
}
