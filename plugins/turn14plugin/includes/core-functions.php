<?php // turn14 - Core Functionality

// disable direct file access
if (!defined('ABSPATH')) {

    exit;

}

function turn14_ajax_get_makes()
{

    // The $_REQUEST contains all the data sent via ajax
    if (isset($_REQUEST)) {

        $year = $_REQUEST['year'];
$fitments = get_terms(array(
    'taxonomy' => 'product_fitments',
    'hide_empty' => false,
));
$fitArray = (string)'';
foreach ($fitments as $fit => $key) {
    if ($fit == 0) {
        $append = '';
    } else {
        $append = ',';
    }
    $append .= $key->name;
    $fitArray .= $append;
}

        // Let's take the data that was sent and do something with it
        $echo = $year;
        $options = get_option('turn14_options', turn14_options_default());
        $vcdb = new wpdb($options['turn14_vcdb_db_user'], $options['turn14_vcdb_db_password'],  $options['turn14_vcdb_db_name'], $options['turn14_vcdb_db_host']);
        $query = "SELECT DISTINCT BaseVehicle.MakeID, Make.MakeName FROM Vehicle INNER JOIN BaseVehicle on Vehicle.BaseVehicleID = BaseVehicle.BaseVehicleId INNER JOIN Make on BaseVehicle.MakeID = Make.MakeID WHERE YearID = " . $year . " AND VehicleID in (".$fitArray.") ORDER BY MakeName";
        $makes = $vcdb->get_results($query);
        $makesString = json_encode($makes);
        // Now we'll return it to the javascript function
        // Anything outputted will be returned in the response
        echo $makesString;

        // If you're debugging, it might be useful to see what was sent in the $_REQUEST

    }

    // Always die in functions echoing ajax content
    die();
}

add_action('wp_ajax_turn14_ajax_get_makes', 'turn14_ajax_get_makes');
add_action('wp_ajax_nopriv_turn14_ajax_get_makes', 'turn14_ajax_get_makes');

function turn14_ajax_get_models()
{

    // The $_REQUEST contains all the data sent via ajax
    if (isset($_REQUEST)) {

        $year = $_REQUEST['year'];
        $make = $_REQUEST['make'];
        $fitments = get_terms(array(
    'taxonomy' => 'product_fitments',
    'hide_empty' => false,
));
$fitArray = (string) '';
foreach ($fitments as $fit => $key) {
    if ($fit == 0) {
        $append = '';
    } else {
        $append = ',';
    }
    $append .= $key->name;
    $fitArray .= $append;
}

        // Let's take the data that was sent and do something with it
        $options = get_option('turn14_options', turn14_options_default());
        $vcdb = new wpdb($options['turn14_vcdb_db_user'], $options['turn14_vcdb_db_password'], $options['turn14_vcdb_db_name'], $options['turn14_vcdb_db_host']);
        $query = "SELECT DISTINCT Model.ModelName, Model.ModelID FROM Vehicle INNER JOIN BaseVehicle on Vehicle.BaseVehicleID = BaseVehicle.BaseVehicleID INNER JOIN Model on BaseVehicle.ModelID = Model.ModelID  WHERE YearID = " . $year . " AND MakeID = " . $make . " AND VehicleID in (".$fitArray.") ORDER BY ModelName";
        $makes = $vcdb->get_results($query);
        $makesString = json_encode($makes);
        // Now we'll return it to the javascript function
        // Anything outputted will be returned in the response
        echo $makesString;

        // If you're debugging, it might be useful to see what was sent in the $_REQUEST

    }

    // Always die in functions echoing ajax content
    die();
}
add_action('wp_ajax_turn14_ajax_get_models', 'turn14_ajax_get_models');
add_action('wp_ajax_nopriv_turn14_ajax_get_models', 'turn14_ajax_get_models');

function turn14_ajax_submit()
{

    // The $_REQUEST contains all the data sent via ajax
    if (isset($_REQUEST)) {

        $year = $_REQUEST['year'];
        $make = $_REQUEST['make'];
        $model = $_REQUEST['model'];
        $fitments = get_terms(array(
            'taxonomy' => 'product_fitments',
            'hide_empty' => false,
        ));
        $fitArray = (string) '';
        foreach ($fitments as $fit => $key) {
            if ($fit == 0) {
                $append = '';
            } else {
                $append = ',';
            }
            $append .= $key->name;
            $fitArray .= $append;
        }

        // Let's take the data that was sent and do something with it
        $options = get_option('turn14_options', turn14_options_default());
        $vcdb = new wpdb($options['turn14_vcdb_db_user'], $options['turn14_vcdb_db_password'], $options['turn14_vcdb_db_name'], $options['turn14_vcdb_db_host']);
        $query = "SELECT * FROM Vehicle INNER JOIN BaseVehicle on Vehicle.BaseVehicleID = BaseVehicle.BaseVehicleID WHERE YearID = " . $year . " AND MakeID = " . $make . " AND ModelID = " . $model . " AND VehicleID in (".$fitArray.")";
        $vehicle = $vcdb->get_results($query);
        $vehicleString = json_encode($vehicle);
        $array = array(
            'url' => get_site_url(),
            'vehicles' => $vehicleString,
        );
        $arrayString = json_encode($array);
        // Now we'll return it to the javascript function
        // Anything outputted will be returned in the response
        echo $arrayString;
    }

    // Always die in functions echoing ajax content
    die();
}
add_action('wp_ajax_turn14_ajax_submit', 'turn14_ajax_submit');
add_action('wp_ajax_nopriv_turn14_ajax_submit', 'turn14_ajax_submit');

function turn14_display_vmm($atts, $content)
{
    $fitments = get_terms(array(
    'taxonomy' => 'product_fitments',
    'hide_empty' => false,
));
    $fitArray = '';
    foreach($fitments as $fit => $key){
        if($fit == 0) {
            $append = '';
        } else {
            $append = ',';
        }
        $append .= $key->name;
        $fitArray .= $append;
    }
    $options = get_option('turn14_options', turn14_options_default());
    $vcdb = new wpdb($options['turn14_vcdb_db_user'], $options['turn14_vcdb_db_password'],  $options['turn14_vcdb_db_name'], $options['turn14_vcdb_db_host']);

    $query = 'SELECT DISTINCT YearID FROM Vehicle INNER JOIN BaseVehicle on Vehicle.BaseVehicleID = BaseVehicle.BaseVehicleId WHERE VehicleID in ('.$fitArray.') ORDER BY YearID DESC';
    $content = '
				<h2>
					Search by car
				</h2>
                <div class="form">
                <select id="year" name="year">
                    <option>CHOOSE A YEAR</option>';
    
    foreach ($vcdb->get_results($query) as $key => $row) {
        $content .= '<option value="' . $row->YearID . '">' . $row->YearID . '</option>';
    }
    $content .= '
                            </select>
					<select id="make" name="make" disabled>
					    <option>CHOOSE A MAKE</option>
                    </select>
                    <select id="model" name="model" disabled>
					    <option>CHOOSE A MODEL</option>
                    </select>
                    <input type="submit" value="Search" id="vmmSubmit">
				</div>
	';
    return $content;

}
add_shortcode('turn14displayvmm', 'turn14_display_vmm');
