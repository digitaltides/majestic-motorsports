<?php
require_once("../../../../wp-load.php");
$brandsRes = [];
$optionsNew = [];
ignore_user_abort(true);
    error_log('start');
    
    $options = get_option('turn14_brands');
    $turn_options = get_option('turn14_options', turn14_options_default());
    $urlRoot = isset($turn_options['turn14_testing']) ? 'https://apitest.turn14.com' : 'https://api.turn14.com';
    $optionsNew = (array) [];
    foreach ($options as $key=>&$brands) {
		echo 'newBrand';
        global $brandsRes;
        $brandsRes = $options;
        $brandsArr = explode(",", $brands);
        if ($brandsArr[1] == 'old') {
            unset($options[$key]);
            continue;
        } else {
			$access_token = get_turn14_accessToken()['access_token'];
            $brands = $brandsArr[0];
            array_push($optionsNew, $brands . ',old');
            $brands = $brandsArr[0];
			unset($options[$key]);
            $url = $urlRoot . "/v1/items/brand/" . $brands . "?access_token=" . $access_token;
            // create curl resource
            $ch = curl_init();

            // set url
            curl_setopt($ch, CURLOPT_URL, $url);

            //return the transfer as a string
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

            // $result contains the output string
            $result = curl_exec($ch);
            $json = json_decode($result, true);
            // close curl resource to free up system resources
            curl_close($ch);
            if ($json['meta']['total_pages'] > 1) {
                $pages = $json['meta']['total_pages'];
                for ($page = 1; $page <= $pages; $page++) {
                    $url = $urlRoot . "/v1/items/brand/" . $brands . "?page=" . $page . "access_token=" . $access_token;
                    // create curl resource
                    $ch = curl_init();

                    // set url
                    curl_setopt($ch, CURLOPT_URL, $url);

                    //return the transfer as a string
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

                    // $result contains the output string
                    $result = curl_exec($ch);
                    $jsonSub = json_decode($result, true);
                    array_push($json, $jsonSub);
                    // close curl resource to free up system resources
                    curl_close($ch);
                }
                foreach ($json['data'] as $k => &$item) {
                    if ($item['type'] == 'Item') {
                        set_time_limit(0);
                        ini_set('max_input_time', 300);
                        turn14_item_data($item, $access_token, $brands);
                        sleep(2);
                    }
                    unset($json['data'][$k]);
                }
            } else {
                foreach ($json['data'] as $k =>  &$item) {
                    if ($item['type'] == 'Item') {
                        set_time_limit(0);
                        turn14_item_data($item, $access_token, $brands);
                        sleep(2);
                    }

                    unset($json['data'][$k]);
                }
            }
        }
		unset($options[$key]);
		array_push($options, $brands . ',old');
        sleep(3);
    }
