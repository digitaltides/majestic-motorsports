<?php
function turn14_get_items() {
    $access_token = get_turn14_accessToken()['access_token'];
    $turn_options = get_option('turn14_options', turn14_options_default());
    $urlRoot = isset($turn_options['turn14_testing']) ? 'https://apitest.turn14.com' : 'https://api.turn14.com';
            $brands = $_REQUEST['brand'];
            $url = $urlRoot . "/v1/items/brand/" . $brands . "?access_token=" . $access_token;
            // create curl resource
            $ch = curl_init();

            // set url
            curl_setopt($ch, CURLOPT_URL, $url);

            //return the transfer as a string
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

            // $result contains the output string
            $result = curl_exec($ch);
            $json = json_decode($result, true);
            // close curl resource to free up system resources
            curl_close($ch);
            if ($json['meta']['total_pages'] > 1) {
                $pages = $json['meta']['total_pages'];
                for ($page = 1; $page <= $pages; $page++) {
                    $url = $urlRoot . "/v1/items/brand/" . $brands . "?page=" . $page . "access_token=" . $access_token;
                    // create curl resource
                    $ch = curl_init();

                    // set url
                    curl_setopt($ch, CURLOPT_URL, $url);

                    //return the transfer as a string
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

                    // $result contains the output string
                    $result = curl_exec($ch);
                    $jsonSub = json_decode($result, true);
                    array_push($json['data'], $jsonSub['data']);
                    // close curl resource to free up system resources
                    curl_close($ch);
                    
                }
                echo json_encode($json['data']);
                wp_die();
            } else {
                echo json_encode($json['data']);
                wp_die();
            }
}
add_action('wp_ajax_turn14_get_items', 'turn14_get_items');

function turn14_insert_product(){
    $itemRAW = $_REQUEST['item'];
    $brand = $_REQUEST['brand'];
    $access_token = get_turn14_accessToken()['access_token'];
    $post_id = turn14_item_data($itemRAW, $access_token, $brand);
    echo $post_id;
    wp_die();
}
add_action('wp_ajax_turn14_insert_product', 'turn14_insert_product');
