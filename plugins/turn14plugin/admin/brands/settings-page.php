<?php // turn14 - Settings Page



// disable direct file access
if ( ! defined( 'ABSPATH' ) ) {
	
	exit;
	
}



// display the plugin settings page
function turn14_display_brands_page() {
	
	// check if user is allowed access
	if ( ! current_user_can( 'manage_options' ) ) return;
	
	?>
	
	<div class="wrap">
		<h1><?php echo esc_html( get_admin_page_title() ); ?></h1>
		<form action="options.php" method="post" id="brands-form">
			
			<?php
			
			// output security fields
			settings_fields( 'turn14_brands' );
			
			// output setting sections
			do_settings_sections( 'turn14_brands' );
			
			// submit button
			submit_button();
			
			?>
		</form>
	</div>
	
	<?php
	
}


