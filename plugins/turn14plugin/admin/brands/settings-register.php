<?php // turn14 - Register Settings



// disable direct file access
if ( ! defined( 'ABSPATH' ) ) {
	
	exit;
	
}

// register plugin settings
function turn14_register_brands() {
	
	/*
	
	register_setting( 
		string   $option_group, 
		string   $option_name, 
		callable $sanitize_callback = ''
	);
	
	*/
	register_setting( 
		'turn14_brands', 
		'turn14_brands', 
		'turn14_callback_validate_options' 
	); 
	

	
	/*
	
	add_settings_section( 
		string   $id, 
		string   $title, 
		callable $callback, 
		string   $page
	);
	
	*/
	add_settings_section(
		'categories',
		'What Categories to Import',
		'turn14_callback_section_categories',
		'turn14_brands'
	);


	
	/*
	
	add_settings_field(
    	string   $id, 
		string   $title, 
		callable $callback, 
		string   $page, 
		string   $section = 'default', 
		array    $args = []
	);
	
	*/
	    // Add Field for selecting the brands to offer
	add_settings_field(  
		'turn14_brands',
		__( 'Select the brands', 'aicp' ),
		'turn14_callback_brands_select', 
		'turn14_brands',
		'categories',
		[ 'id' => 'turn14_brands', 'label' => esc_html__('Brands', 'turn14'), 'cats' => [['termID' => 'MICHELLIN', 'termName' => 'Michellin'], ['termID'=> 'TEST2', 'termName' => 'Test 2']] ]

	); // id, title, display cb, page, section
} 
add_action( 'admin_init', 'turn14_register_brands' );


