<?php // turn14 - Settings Callbacks



// disable direct file access
if ( ! defined( 'ABSPATH' ) ) {
	
	exit;
	
}



// callback: basic information section
function turn14_callback_section_basic() {
	
	echo '<p>'. esc_html__('Please set these basic categories', 'turn14') .'</p>';
	
}



// callback: categories section
function turn14_callback_section_categories() {
echo '<p>' . esc_html__('Each night at midnight, the new categories selected here will import.', 'turn14') . '</p>';

}

function turn14_callback_brands_select( $args ) {
	$options    = get_option('turn14_brands', turn14_brands_default() );
	$optionsSettings = get_option('turn14_options', turn14_options_default());
    $pag        = 2;
	$html       = '';
	$access_token = get_turn14_accessToken()['access_token'];
	if($access_token){
$urlRoot = isset($optionsSettings['turn14_testing']) ? 'https://apitest.turn14.com' : 'https://api.turn14.com';

	$_cats = get_brands_turn14($urlRoot, $access_token);
	if($options) {
		foreach ($_cats as $term) {
		$checked = isset( $options[$term['termID']] ) ? 'checked="checked"' : '';
		$new = isset( $options[$term['termID']] ) ? ',old' : ',new';
		$html .= '<input type="checkbox" id="turn14_brands_'.$term['termID'].'" name="turn14_brands['.$term['termID'] .']" value="'.$term['termID'].$new.'" '. $checked .' />';
        $html .= '<label for="turn14_brands_'.$term['termID'] .'">'.$term['termName'].'</label><br>';
    	}
	} else {
		foreach ($_cats as $term) {
			$html .= '<input type="checkbox" id="turn14_brands_'.$term['termID'].'" name="turn14_brands['.$term['termID'] .']" value="'.$term['termID'].',new" />';
        	$html .= '<label for="turn14_brands_'.$term['termID'] .'">'.$term['termName'].'</label><br>';
    	}
	}


    echo $html;

	}
	
}


