<?php // turn14 - Settings Callbacks



// disable direct file access
if ( ! defined( 'ABSPATH' ) ) {
	
	exit;
	
}




// callback: vcdb section
function turn14_callback_section_vcdb() {
	
	echo '<p>'. esc_html__('Please set these to the data given when purchasing this plugin.', 'turn14') .'</p>';
	
}


// callback: text field
function turn14_callback_field_text( $args ) {
	$options = get_option( 'turn14_options', turn14_options_default() );
	$id    = isset( $args['id'] )    ? $args['id']    : '';
	$label = isset( $args['label'] ) ? $args['label'] : '';
	$value = isset( $options[$id] ) ? sanitize_text_field( $options[$id] ) : '';
	
	echo '<input id="turn14_options_'. $id .'" name="turn14_options['. $id .']" type="text" size="40" value="'. $value .'"><br />';
	echo '<label for="turn14_options_'. $id .'">'. $label .'</label>';
}



// radio field options
function turn14_options_radio() {
	
	return array(
		
		'enable'  => esc_html__('Enable custom styles', 'turn14'),
		'disable' => esc_html__('Disable custom styles', 'turn14')
		
	);
	
}



// callback: radio field
function turn14_callback_field_radio( $args ) {
	
	$options = get_option( 'turn14_options', turn14_options_default() );
	
	$id    = isset( $args['id'] )    ? $args['id']    : '';
	$label = isset( $args['label'] ) ? $args['label'] : '';
	
	$selected_option = isset( $options[$id] ) ? sanitize_text_field( $options[$id] ) : '';
	
	$radio_options = turn14_options_radio();
	
	foreach ( $radio_options as $value => $label ) {
		
		$checked = checked( $selected_option === $value, true, false );
		
		echo '<label><input name="turn14_options['. $id .']" type="radio" value="'. $value .'"'. $checked .'> ';
		echo '<span>'. $label .'</span></label><br />';
		
	}
	
}



// callback: textarea field
function turn14_callback_field_textarea( $args ) {
	
	$options = get_option( 'turn14_options', turn14_options_default() );
	
	$id    = isset( $args['id'] )    ? $args['id']    : '';
	$label = isset( $args['label'] ) ? $args['label'] : '';
	
	$allowed_tags = wp_kses_allowed_html( 'post' );
	
	$value = isset( $options[$id] ) ? wp_kses( stripslashes_deep( $options[$id] ), $allowed_tags ) : '';
	
	echo '<textarea id="turn14_options_'. $id .'" name="turn14_options['. $id .']" rows="5" cols="50">'. $value .'</textarea><br />';
	echo '<label for="turn14_options_'. $id .'">'. $label .'</label>';
	
}



// callback: checkbox field
function turn14_callback_field_checkbox( $args ) {
	
	$options = get_option( 'turn14_options', turn14_options_default() );
	
	$id    = isset( $args['id'] )    ? $args['id']    : '';
	$label = isset( $args['label'] ) ? $args['label'] : '';
	
	$checked = isset( $options[$id] ) ? checked( $options[$id], 1, false ) : '';
	
	echo '<input id="turn14_options_'. $id .'" name="turn14_options['. $id .']" type="checkbox" value="1"'. $checked .'> ';
	echo '<label for="turn14_options_'. $id .'">'. $label .'</label>';
	
}



// select field options
function turn14_options_select() {
	
	return array(
		
		'default'   => esc_html__('Default',   'turn14'),
		'light'     => esc_html__('Light',     'turn14'),
		'blue'      => esc_html__('Blue',      'turn14'),
		'coffee'    => esc_html__('Coffee',    'turn14'),
		'ectoplasm' => esc_html__('Ectoplasm', 'turn14'),
		'midnight'  => esc_html__('Midnight',  'turn14'),
		'ocean'     => esc_html__('Ocean',     'turn14'),
		'sunrise'   => esc_html__('Sunrise',   'turn14'),
		
	);
	
}



// callback: select field
function turn14_callback_field_select( $args ) {
	
	$options = get_option( 'turn14_options', turn14_options_default() );
	
	$id    = isset( $args['id'] )    ? $args['id']    : '';
	$label = isset( $args['label'] ) ? $args['label'] : '';
	
	$selected_option = isset( $options[$id] ) ? sanitize_text_field( $options[$id] ) : '';
	
	$select_options = turn14_options_select();
	
	echo '<select id="turn14_options_'. $id .'" name="turn14_options['. $id .']">';
	
	foreach ( $select_options as $value => $option ) {
		
		$selected = selected( $selected_option === $value, true, false );
		
		echo '<option value="'. $value .'"'. $selected .'>'. $option .'</option>';
		
	}
	
	echo '</select> <label for="turn14_options_'. $id .'">'. $label .'</label>';
	
}


