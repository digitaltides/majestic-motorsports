
jQuery(document).ready(function ($) {
    $('#brands-form').submit(function(e) {
                e.preventDefault();
        var brandsTotal = 0;
        var brandsCurr = 0;
        var itemsTotal = 0;
        var itemsCurr = 0;
        var checkedValues = $('#brands-form input:checkbox:checked').map(function() {
            return this.value;
        }).get();
        $('#brands-form').empty();
        $('#brands-form').append('<div style="width: 100%; background-color: grey; "><div id="brands-progress" style="background-color: green; height: 3vh;;"><p id="turn-progress" style="text-align: center; color: white;line-height: 3vh;"><span id="brandsCurr">'+brandsCurr+'</span>/<span id="brandsTotal">'+brandsTotal+'</p></div></div>');
        $('#brands-form').append('<div style="width: 100%; background-color: grey; "><div id="item-progress" style="background-color: green; margin-top: 10px; height: 3vh;"><p id="turn-progressi" style="text-align: center; color: white;line-height: 3vh;"><span id="itemsCurr">'+itemsCurr+'</span>/<span id="itemsTotal">'+itemsTotal+'</span></div></div>');
        var itemselem = document.getElementById("item-progress"); 
        var brandselem = document.getElementById("brands-progress"); 
        brandsTotal = checkedValues.length;
        $('#brandsTotal').text(brandsTotal);
        for(var i = 0; i <= checkedValues.length -1; i++) {
            var brand = checkedValues[i];
            brand = brand.split(',');
            brandCurr = i+1;
            $('#brandsCurr').text(brandCurr);
             brandselem.style.width = (brandsCurr/brandsTotal)*100 + '%'; 
                $.ajax({
                    url: turn_ajax.ajax_url,
                    async: false,
                    data: {
                        action: 'turn14_get_items',
                        brand: brand[0] 
                    },
                    success: function(data) {
                        data = JSON.parse(data);
                        itemsTotal = data.length;
                        $('#itemsTotal').text(itemsTotal)
                        for(var d = 0; d <= data.length - 1; d++) {
                            console.log(data[d])
                            $.ajax({
                                url: turn_ajax.ajax_url,
                                data:{
                                    action: 'turn14_insert_product',
                                    item: data[d],
                                    brand: brand[0]
                                },
                                async: false,
                                success: function(data){
                                    console.log('items: '+data);
                                    var i = data.length -1
                                    itemsCurr = d+1;
                                    itemselem.style.width = (itemsCurr/itemsTotal)*100 + '%'; 
                                    $('#itemsCurr').text(itemsCurr);
                                    setTimeout(function(){
                                        console.log('held');
                                    }, 2000)
                                }
                            })

                        }
                    },
                    error: function(err){
                        console.log(err)
                    }
                })
        }

    })
});