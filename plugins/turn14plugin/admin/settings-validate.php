<?php // turn14 - Validate Settings



// disable direct file access
if ( ! defined( 'ABSPATH' ) ) {
	
	exit;
	
}



// callback: validate options
function turn14_callback_validate_options( $input ) {
	$firstRun = get_option('firstRun');
	if($firstRun == 0) {
		update_option('firstRun', '1');
	}
	
	
	// custom style
	$radio_options = turn14_options_radio();
	

		
	return $input;
	
}


