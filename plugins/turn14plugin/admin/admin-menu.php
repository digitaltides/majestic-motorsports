<?php // turn14 - Admin Menu



// disable direct file access
if ( ! defined( 'ABSPATH' ) ) {
	
	exit;
	
}




// add top-level administrative menu
function turn14_add_toplevel_menu() {
	
	/* 
	
	add_menu_page(
		string   $page_title, 
		string   $menu_title, 
		string   $capability, 
		string   $menu_slug, 
		callable $function = '', 
		string   $icon_url = '', 
		int      $position = null 
	)
	
	*/
	
	add_submenu_page(
    'woocommerce',
    'Brands',
    'Brands',
    'manage_options',
    'turn14_brands',
    'turn14_display_brands_page'
);
	add_submenu_page(
    'woocommerce',
    'Turn 14 Settings',
    'Turn 14 Settings',
    'manage_options',
    'turn14_settings',
    'turn14_display_settings_page'
);
	
}
 add_action( 'admin_menu', 'turn14_add_toplevel_menu' );


