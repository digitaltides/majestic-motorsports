<?php // turn14 - Register Settings



// disable direct file access
if ( ! defined( 'ABSPATH' ) ) {
	
	exit;
	
}



// register plugin settings
function turn14_register_settings() {
	
	/*
	
	register_setting( 
		string   $option_group, 
		string   $option_name, 
		callable $sanitize_callback = ''
	);
	
	*/
	register_setting( 
		'turn14_options', 
		'turn14_options', 
		'turn14_callback_validate_options' 
	); 
	
	

	
	/*
	
	add_settings_section( 
		string   $id, 
		string   $title, 
		callable $callback, 
		string   $page
	);
	
	*/
	add_settings_section(
		'basicinfo',
		'Basic Information',
		'turn14_callback_section_basic',
		'turn14'
	);
	add_settings_section(
		'vcdb',
		'VCDB Information',
		'turn14_callback_section_vcdb',
		'turn14'
	);


	
	/*
	
	add_settings_field(
    	string   $id, 
		string   $title, 
		callable $callback, 
		string   $page, 
		string   $section = 'default', 
		array    $args = []
	);
	
	*/
	add_settings_field(
		'turn14_client_id',
		esc_html__('Client ID', 'turn14'),
		'turn14_callback_field_text',
		'turn14', 
		'basicinfo', 
		[ 'id' => 'turn14_client_id', 'label' => esc_html__('Turn 14 Client ID', 'turn14') ]
	);
	add_settings_field(
		'turn14_client_secret',
		esc_html__('Client Secret', 'turn14'),
		'turn14_callback_field_text',
		'turn14', 
		'basicinfo', 
		[ 'id' => 'turn14_client_secret', 'label' => esc_html__('Turn 14 Client ID', 'turn14')]
	);
	add_settings_field(
		'turn14_pricing',
		esc_html__('Markup', 'turn14'),
		'turn14_callback_field_text',
		'turn14', 
		'basicinfo', 
		[ 'id' => 'turn14_pricing', 'label' => esc_html__('Please insert as a percent w/o the % sign.', 'turn14')]
	);
	add_settings_field(
		'turn14_testing',
		esc_html__('Test Mode', 'turn14'),
		'turn14_callback_field_checkbox',
		'turn14',
		'basicinfo',
		['id' => 'turn14_testing', 'label' => esc_html__('Enable test mode for the Turn 14 API ', 'turn14')]
	);


	add_settings_field(
		'turn14_vcdb_db_user',
		esc_html__('Database User', 'turn14'),
		'turn14_callback_field_text',
		'turn14', 
		'vcdb', 
		[ 'id' => 'turn14_vcdb_db_user', 'label' => esc_html__('Please enter the information that is given to you.', 'turn14')]
	);
	add_settings_field(
		'turn14_vcdb_db_password',
		esc_html__('Database Password', 'turn14'),
		'turn14_callback_field_text',
		'turn14', 
		'vcdb', 
		[ 'id' => 'turn14_vcdb_db_password', 'label' => esc_html__('Please enter the information that is given to you.', 'turn14')]
	);
	add_settings_field(
		'turn14_vcdb_db_host',
		esc_html__('Database Host', 'turn14'),
		'turn14_callback_field_text',
		'turn14', 
		'vcdb', 
		[ 'id' => 'turn14_vcdb_db_host', 'label' => esc_html__('Please enter the information that is given to you.', 'turn14')]
	);
	add_settings_field(
		'turn14_vcdb_db_name',
		esc_html__('Database Name', 'turn14'),
		'turn14_callback_field_text',
		'turn14', 
		'vcdb', 
		[ 'id' => 'turn14_vcdb_db_name', 'label' => esc_html__('Please enter the information that is given to you.', 'turn14')]
	);
	
} 
add_action( 'admin_init', 'turn14_register_settings' );


